package com.spacex.java8semesteranroid;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.QuickContactBadge;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private EditText editUsername,editPassword;
    private AppCompatButton loginBtn;
    private TextView clickShow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_form_layout);
        initToolBar();
        findViews();
    }
    private void findViews(){
        editUsername=findViewById(R.id.edit_username);
        editPassword=findViewById(R.id.edit_password);
        clickShow=findViewById(R.id.text_click);
        loginBtn=findViewById(R.id.button_login);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editUsername.getText();
            }
        });

//        next way to register source btn
        loginBtn.setOnClickListener(this);
    }
    public void initToolBar(){
        toolbar=findViewById(R.id.toolbarid);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Vedas College");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//show the back button
        getSupportActionBar().setDisplayShowTitleEnabled(true);//show toolbar title
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.button_login){
            String username=editUsername.getText().toString();
            String password=editPassword.getText().toString();
            Log.d("username",username+" "+password);
            clickShow.setText("Username"+username+ "password"+password);
        }
    }
}
